#!/usr/bin/env bash

currentDate=$(date +"%T")

helm package *-configmap
helm repo index ./ --url https://edimoto.bitbucket.io/

git add .
git commit -m "Saving changes at $currentDate"
git push origin master