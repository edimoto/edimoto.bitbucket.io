#!/usr/bin/env bash

currentDate=$(date +"%T")
helm package *-configmap
helm repo index ./ --url https://edimoto.bitbucket.io/cod/
git add .
git commit -m "Generating new distribution at $currentDate"
git push origin master